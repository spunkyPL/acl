(function(){
  angular
    .module('acl')
    .filter('teamsFilter', teamsFilter);

    teamsFilter.$inject = ['DataManager'];

    function teamsFilter(DataManager){
      return function(teamID) {
        var teamName = null;

        for(var i = 0; i < DataManager.teams.length; i++) {
          if (DataManager.teams[i].id == teamID) {
            teamName = DataManager.teams[i].name;
            return teamName;
          }
        }

        console.log(teamID);
        return "Error: There isn't any team with specified id";
      }
    }
})();
