(function(){
  angular
    .module('acl')
    .filter('driversByActiveTeamFilter', driversByActiveTeamFilter);

    driversByActiveTeamFilter.$inject = ['MetricsManager'];

    function driversByActiveTeamFilter(MetricsManager){
      return function(drivers) {
        var filteredDrivers = [];

        for(var i = 0; i < drivers.length; i++)
        {
          if(drivers[i].team == MetricsManager.activeTeam) {
            filteredDrivers.push(drivers[i]);
          }
        }

        return filteredDrivers;
      }
    }
})();
