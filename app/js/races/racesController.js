(function() {
  angular
    .module('acl')
    .controller('RacesController', RacesController);

    RacesController.$inject = ['MetricsManager', 'DataManager'];

    function RacesController(MetricsManager, DataManager) {
      var vm = this;

      vm.MetricsManager = MetricsManager;
      vm.DataManager = DataManager;
    }

})();
