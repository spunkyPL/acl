(function() {
  angular
    .module('acl')
    .controller('TeamsController', TeamsController);

    TeamsController.$inject = ['MetricsManager', 'DataManager']

    function TeamsController(MetricsManager, DataManager) {
      var vm = this;

      vm.MetricsManager = MetricsManager;
      vm.DataManager = DataManager;
    }
})();
