(function(){
  angular
    .module('acl')
    .controller('DriversController', DriversController);

    DriversController.$inject = ['MetricsManager', 'DataManager'];

    function DriversController(MetricsManager, DataManager) {
      var vm = this;

      vm.MetricsManager = MetricsManager;
      vm.DataManager = DataManager;

      vm.test = "Hello world";
    }
})();
