(function (){
  angular
    .module('acl')
    .factory('MetricsManager', MetricsManager);

    function MetricsManager() {
      var metricsObj = {
        driversActive: true,
        racesActive: false,
        teamsActive: false,
        activeTeam: undefined,
        setActiveTeam: setActiveTeam,
        activatePage: activatePage
      };

      return metricsObj;

      function activatePage(page) {
        switch (page) {
          case 'drivers':
            deactivateAllPages();
            metricsObj.driversActive = true;
            break;

          case 'races':
            deactivateAllPages();
            metricsObj.racesActive = true;
            break;

          case 'teams':
            deactivateAllPages();
            metricsObj.teamsActive = true;
            break;
        }
      }

      function deactivateAllPages() {
        metricsObj.driversActive = false;
        metricsObj.racesActive = false;
        metricsObj.teamsActive = false;
      }

      function setActiveTeam(teamID) {
        metricsObj.activeTeam = teamID;
      }
    }
})();
