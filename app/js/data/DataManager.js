(function(){
  angular
    .module('acl')
    .factory('DataManager', DataManager);

    DataManager.$inject = ['$http']

    function DataManager($http) {
      var dataObj = {
        drivers: [],
        teams: [],
        races: races,
        getTeam: getTeam,
        getDriver: getDriver
      };

      $http({
        method : 'GET',
        url : 'api/api.php?action=getDrivers',
      }).then(function(response) {
         dataObj.drivers = response.data;
      }, function(response) {
        alert('error');
      });

      $http({
        method : 'GET',
        url : 'api/api.php?action=getTeams',
      }).then(function(response) {
         dataObj.teams = response.data;
      }, function(response) {
        alert('error');
      });

      return dataObj;
    }

/*    //mocked data
    var drivers = [
      {id: 1, name: 'spunky', wykopNickname: 'spunky', team: 0},
      {id: 2, name: 'Golomp', wykopNickname: 'Golomp', team: 0},
      {id: 3, name: 'Valentino Rossi', wykopNickname: 'VR46', team: 1},
      {id: 4, name: 'Patyk', wykopNickname: 'Patyk15', team: 1},
      {id: 5, name: 'RealAKP', wykopNickname: 'RealAKP', team: 2},
      {id: 6, name: 'Wuza1337', wykopNickname: 'Wuza1337', team: 2},
      {id: 7, name: 'Batonik', wykopNickname: 'tenbatonik', team: 2},
      {id: 8, name: 'Damian TheSznikers', wykopNickname: 'TheSznikers', team: 3}
    ];

    //mocked data
    var teams = [
      {id: 1, name: 'Parabolica Racing Team'},
      {id: 2, name: 'The Doctor Racing Team'},
      {id: 3, name: 'RWB Racing Team'},
      {id: 4, name: 'The Sznikers Racing Team'}
    ];
*/

    //mocked data
    var races = [
      {id: 1, name: 'Zandvoort', date: '2016-12-21'},
      {id: 2, name: 'Tor Poznań', date: '2016-11-12'},
      {id: 3, name: 'Barcelona', date: '2016-10-09'}
    ]

    function getTeam(teamID) {
      for(var i = 0; i < teams.length; i++) {
        if(teams[i].id == teamID) {
          return teams[i];
        }
      }
    }

    function getDriver(driverID) {
      for(var i = 0; i < drivers.length; i++) {
        if(drivers[i].id === driverID) {
          return driver[i];
        }
      }
    }
})();
