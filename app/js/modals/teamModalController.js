(function(){
  angular
    .module('acl')
    .controller('TeamModalController', TeamModalController);

    TeamModalController.$inject = ['MetricsManager', 'DataManager']

    function TeamModalController(MetricsManager, DataManager) {
      var vm = this;

      this.MetricsManager = MetricsManager;
      this.DataManager = DataManager;
    }
})();
