(function(){
  angular
    .module('acl')
    .controller('NavigationController', NavigationController);

    NavigationController.$inject = ['MetricsManager'];

    function NavigationController(MetricsManager) {
      var vm = this;

      vm.MetricsManager = MetricsManager;
      vm.activatePage = MetricsManager.activatePage;
    }
})();
