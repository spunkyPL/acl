<?php

$db = new PDO('mysql:host=localhost;dbname=acl;charset=utf8', 'root');

if (($_SERVER['REQUEST_METHOD'] == 'GET' && isset($_GET['action']))) {
  switch ($_GET['action']) {
    case 'getDrivers':
      getDrivers($db);
      break;

    case 'getTeams':
      getTeams($db);
      break;

    default:
      break;
  }
}

function getDrivers($db) {
  $result = $db->query('SELECT * FROM drivers');
  $result = $result->fetchAll(PDO::FETCH_ASSOC);

  header('Content-Type: application/json');
  echo json_encode($result);
}

function getTeams($db) {
  $result = $db->query('SELECT * FROM teams');
  $result = $result->fetchAll(PDO::FETCH_ASSOC);

  header('Content-Type: application/json');
  echo json_encode($result);
}


?>
