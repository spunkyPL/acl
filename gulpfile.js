var gulp = require('gulp');
var concat = require('gulp-concat');
var runSequence = require('run-sequence');
var sass = require('gulp-sass');

var scripts = [
  './app/js/acl.module.js',
  './app/js/**/*.js'
];

var main_html = [
  './app/index.html'
];

var styles = [
  './app/styles/**/*.sass'
];

var views = [
  '!./app/index.html',
  './app/**/*.html'
];

gulp.task('scripts', function() {
  return gulp
    .src(scripts)
    .pipe(concat('acl.js'))
    .pipe(gulp.dest('./dist/'));
});

gulp.task('html', function() {
  return gulp
    .src(main_html)
    .pipe(gulp.dest('./dist/'));
});

gulp.task('views', function() {
  return gulp
    .src(views, {base: './app/js/'})
    .pipe(gulp.dest('./dist/views/'));
});

gulp.task('styles', function() {
  return gulp
    .src(styles)
    .pipe(sass().on('error', sass.logError))
    .pipe(gulp.dest('./dist/css/'));
});

gulp.task('make-dist', function(callback) {
  runSequence(
    'html',
    'scripts',
    'styles',
    'views',
    function (error) {
      if (error) {
        console.log(error.message);
      } else {
        console.log('Dist is ready to upload');
      }
      callback(error);
    }
  );
});

gulp.task('send-to-server', function() {
  return gulp.src('dist/**/*')
          .pipe(gulp.dest('D:/http/'));
});


gulp.task('make-and-serve', function (callback) {
  runSequence(
    'make-dist',
    'send-to-server',
    function (error) {
      if (error) {
        console.log(error.message);
      } else {
        console.log('Uploaded to server');
      }
      callback(error);
    });
});
